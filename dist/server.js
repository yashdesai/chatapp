"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const socketio = require('socket.io');
const app = (0, express_1.default)();
const server = http_1.default.createServer(app);
const io = socketio(server);
const PORT = process.env.PORT || 3000;
const publicDir = path_1.default.join(__dirname, '../public');
app.use(express_1.default.static(publicDir));
io.on('connection', () => {
    console.log('New Websocket connection');
});
server.listen(PORT, () => {
    console.log(`Server is up on port ${PORT}`);
});
