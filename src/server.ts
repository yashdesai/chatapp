import path from 'path';
import http from 'http';
import express from 'express';
import { generateMessage } from './utils/messages';
import { User } from './utils/users';
const socketio = require('socket.io')
const Users = new User();

const app = express();
const server = http.createServer(app);
const io = socketio(server);
 
const PORT = process.env.PORT || 5000;
const publicDir = path.join(__dirname, '../public')

app.use(express.static(publicDir));

io.on('connection', (socket: any) => {
    console.log('New Websocket connection');

    socket.on('join', (options: any, callback: any) => {

        const { error, user } = Users.addUser({ id: socket.id, ...options})

        if(error) {
            return callback(error);
        }

        socket.join(user?.room)

        socket.emit('message', generateMessage('Admin','Welcome'));
        socket.broadcast.to(user?.room).emit('message', generateMessage('Admin',`${user?.username} has joined the room.`));

        io.to(user?.room).emit('roomData', {
            room: user?.room,
            users: Users.getUsersInRoom(user?.room)
        })

        callback();
    })

    socket.on('sendMessage', (message: any, callback: any) => {
        const user = Users.getUser(socket.id)

        io.to(user.room).emit('message', generateMessage(user.username,message)); 
        callback('Delivered');
    });
 
    socket.on('disconnect', () => {

        const user = Users.removeUser(socket.id)

        if(user) {
            io.to(user.room).emit('message', generateMessage('Admin',`${user.username} has left`));
            io.to(user.room).emit('roomData', {
                room: user.room,
                users: Users.getUsersInRoom(user.room)
            })
        }
    })
})

server.listen(PORT, () => {
    console.log(`Server is up on port ${PORT}`);
})
