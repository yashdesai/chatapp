export const generateMessage = (username: any,text: any) => {
    return {
        username,
        text,
        createdAt: new Date().getTime()
    }
}