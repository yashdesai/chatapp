export class User {

    users:any = [];

    public addUser = ({id, username, room}: any) => {
        username = username.trim().toLowerCase()
        room = room.trim().toLowerCase()

        //validate
        if(!username || !room) {
            return {
                error: 'username and room are required'
            }
        }

        //check for existing user
        const existingUser = this.users.find((user: any) => {
            return user.room === room && user.username === username;
        })

        //validate username
        if(existingUser) {
            return {
                error: 'Username is in use'
            }
        }

        //store user
        const user = { id, username, room};
        this.users.push(user);
        return { user }
    }

    public removeUser = (id: any) => {
        const index = this.users.findIndex((user: any) => user.id === id)
        if(index !== -1) {
            return this.users.splice(index, 1)[0]
        }
    }

    public getUser = (id: any) => {
        return this.users.find((user: any) => user.id === id)
    }

    public getUsersInRoom = (room: any) => {
        return this.users.filter((user: any) => user.room === room)
    }
}